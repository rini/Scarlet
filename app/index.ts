/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { app, BrowserWindow, ipcMain, nativeImage, Tray } from "electron";
import { readFile, watch } from "fs/promises";
import { join } from "path";

const js = join(__dirname, "client.js");
const css = join(__dirname, "client.css");

const watchCss = async () => {
    if (app.isPackaged)
        return;
    for await (const { eventType } of watch(css)) {
        if (eventType !== "change")
            continue;
        ipcMain.emit("reload-css", await readFile(css));
    }
};

if (app.requestSingleInstanceLock()) {
    app.whenReady().then(() => {
        const icon = nativeImage.createFromPath(join(__dirname, "icon.png"));
        const tray = new Tray(icon);

        tray.setToolTip("Scarlet Desktop");
        tray.on("click", () => w.restore());

        const w = new BrowserWindow({
            title: "Scarlet Desktop",
            width: 1200,
            height: 800,
            icon,
            autoHideMenuBar: true,
            webPreferences: { preload: join(__dirname, "preload.js") },
        });

        app.on("second-instance", () => w.isMinimized() && w.restore());

        ipcMain.handle("read-js", () => readFile(js, "utf8"));
        ipcMain.handle("read-css", () => readFile(css, "utf8"));

        if (!SC_RELEASE)
            watchCss();

        w.loadURL("https://tetr.io");
    });
} else {
    app.quit();
}
