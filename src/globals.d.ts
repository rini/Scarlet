/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

declare global {
    const SC_VERSION: string;
    const SC_RELEASE: boolean;
    const SC_GIT_FULL: string;
    const SC_GIT_SHORT: string;

    const PIXI: typeof import("pixi.js");
}

export {};
