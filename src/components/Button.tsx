/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { JSX, ParentProps } from "solid-js";

export default (props: ParentProps<{
    callback: JSX.EventHandlerUnion<HTMLDivElement, MouseEvent>;
    class?: string;
}>) =>
    <div
        class={`control_button rg_target_pri ${props.class ?? ""}`}
        data-hover="tap" data-hit="click"
        onClick={props.callback}
    >
        {props.children}
    </div>;


