/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import "./styles.css";

import { Authors, Builtins } from "api";
import { nuke } from "api/store";
import { renderDialog, renderProfilePopup, switchMenu } from "api/ux";
import { match, mount, patch, Plugins, render, setSettings, settings } from "client";
import Button from "components/Button";
import { For, ParentProps } from "solid-js";
import { Dynamic } from "solid-js/web";
import { Author, definePlugin, Plugin } from "types";
import { bre } from "utils";

const REPO_URL = "https://codeberg.org/rini/Scarlet";
const COMMIT_URL = `${REPO_URL}/commit/${SC_GIT_FULL}`;

interface Badge {
    id: string;
    label: string;

    // own extension
    src?: string;
    url?: string;
}

interface User {
    _id: string;
    badges: Badge[];
}

// *un`const`s your type*
const authorIds = new Set<string>(Object.values(Authors));

const SettingsButton =
    <div
        class="scroller_item scroller_item_config has_description ns rg_target_pri"
        onClick={() => switchMenu("config_scarlet")}
        data-hover="hover" data-hit="hit2"
    >
        <h1>SCARLET</h1>
        <p>configure SCARLET</p>
    </div>;

const Link = (props: ParentProps<{ url: string }>) =>
    <a class="bodylink" href={props.url} target="_blank">{props.children}</a>;

const PluginAuthor = (props: { author: Author }) =>
    <img
        class="avatar sc-author"
        src={`/user-content/avatars/${Authors[props.author]}.jpg?rv=0`}
        alt={props.author} title={props.author}
        onClick={() => renderProfilePopup({ userID: Authors[props.author] })}
    />;

const PluginSettings = (props: { id: string; plug: Plugin }) =>
    <div class="scroller_block">
        <h1
            class="checkbox rg_target_pri"
            classList={{
                checked: Builtins.has(props.id) || settings().enabledPlugins.has(props.id),
                disabled: Builtins.has(props.id),
            }}
            onClick={() => setSettings((s) => {
                if (!s.enabledPlugins.delete(props.id))
                    s.enabledPlugins.add(props.id);
                return s;
            })}
            data-hover="tap" data-hit="click"
        >
            {props.plug.name.toUpperCase()}
        </h1>
        <div class="button_tr_h">
            <For each={props.plug.authors}>
                {(author) => <PluginAuthor author={author} />}
            </For>
        </div>
        <p>{props.plug.description}</p>
        <Dynamic component={props.plug.settings?.bind(props.plug)} />
    </div>;

const SettingsMenu = () =>
    <div class="right_scroller ns" data-menuview="config_scarlet">
        <div class="scroller_block">
            <h1>Scarlet {SC_VERSION}</h1>
            <p>
                Welcome to Scarlet! You are currently on commit <Link url={COMMIT_URL}>{SC_GIT_SHORT}</Link> <br />
                <br />
                Scarlet lives on <Link url={REPO_URL}>{Math.random() < 0.2 ? "Codeburger" : "Codeberg"}</Link>,
                contributions are welcome!
            </p>
        </div>
        <div class="scroller_block zero">
            <div class="button_tr_h">
                <Button class="button_tr_i danger" callback={() => renderDialog({
                    title: "RESET SETTINGS?",
                    msg: 'this will instantly delete all data from SCARLET and restart <span class="cheeky">TETRIO</span>',
                    classes: ["crash_modal"],
                    buttons: [
                        {
                            label: "CANCEL",
                            classes: [],
                            callback: (done) => done(),
                        },
                        {
                            label: "DO IT!",
                            classes: ["pri"],
                            callback() {
                                nuke();
                                location.reload();
                            },
                        },
                    ],
                })}>
                    reset settings
                </Button>
            </div>
        </div>
        <For each={Object.entries(Plugins)}>
            {([id, plug]) =>
                // hide builtin plugins if they do not have settings
                (!Builtins.has(id) || plug.settings) && <PluginSettings id={id} plug={plug} />}
        </For>
    </div>;

export default definePlugin({
    name: "Scarlet UX",
    description: "Scarlet's UI/UX stuff",
    authors: ["Rini"],

    start() {
        // keyboard navigation info
        patch(
            bre`config_account:\({starter:"f!.rg_target_pri"\.\*\?}\),config_`,
            "config_scarlet:$1,$&"
        );
        // menu metadata
        patch(
            'config_account:{back:"config",',
            'config_scarlet:{back:"config",header:"CONFIG / SCARLET",footer:"<span style=\\"font-family:PFW\\">meow</span>"},$&'
        );
        // badges!
        patch(
            bre`.get(\`/api/users/\.{\i(\i.userID)}\`).then((\(\i\)=>{`,
            "$&$this.addBadges($1);"
        );
        patch(
            bre`\(\i.classList.add("tetra_badge"),\i.src=\)\(\`/res/badges/\.{\(\i\).id\.\*\?appendChild\)(\(\i\))`,
            "$1$3.src??$2($this.addBadgeUrl($4,$3))"
        );

        // grab some functions
        const switchMenu = match(
            bre`function \(\i\)(\i,\i){(\i[\i].onexit||(()=>{})`
        )?.[1];
        const renderProfile = match(
            bre`function \(\i\)(\i){\.\{1,64\}("oob_modal"),\i.classList.add("tetra_modal")`
        )?.[1];
        const renderDialog = match(
            bre`function \(\i\)(\i){\.\{1,128\}if(\i.classList.add("oob_modal"),`
        )?.[1];
        const showNotification = match(
            bre`function \(\i\)(\i){let \i=\i;\.\{1,64\}notifications.suppress`
        )?.[1];
        patch(
            bre`})();\$`,
            `;Scarlet.Api.UX._init(${switchMenu},${renderProfile},${renderDialog},${showNotification})$&`,
        );

        mount(SettingsButton, { target: "[data-menuview=config]", anchor: "#config_account" });
        render(SettingsMenu, { target: "#menus" });
    },

    addBadges({ user }: { user: User }) {
        user.badges ??= [];
        if (authorIds.has(user._id)) {
            user.badges.push({
                id: "contributor",
                label: "Contributed to Scarlet",
                src: "https://cdn.discordapp.com/emojis/1105406110724268075.webp?quality=lossless",
                url: REPO_URL,
            });
        }
    },

    addBadgeUrl: (element: HTMLImageElement, { url }: Badge) =>
        url ? <a href={url} target="_blank">{element}</a> : element,
});
