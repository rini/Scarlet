/*
 * Scarlet, a client mod for TETR.IO <https://codeberg.org/rini/Scarlet>
 * Copyright (c) 2023 Rini and contributors
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

export function debounce<A extends any[], F extends (...a: A) => void>(f: F, wait: number) {
    let timeout: ReturnType<typeof setTimeout>;
    return ((...a: A) => {
        clearTimeout(timeout);
        timeout = setTimeout(f, wait, ...a);
    }) as F;
}

export function logger(tag: string) {
    const makeLogger = (level: "debug" | "info" | "log" | "warn" | "error") =>
        (msg: string, ...objects: any[]) =>
            console[level](
                `%c Scarlet %c ${tag} %c ${msg}`,
                "background:#e4353c;color:#fff;font-weight:bold",
                "background:#e35157;color:#fff",
                "",
                ...objects);

    return {
        /** Log a message with `console.debug` */
        d: makeLogger("debug"),
        /** Log a message with `console.error` */
        e: makeLogger("error"),
        /** Log a message with `console.info` */
        i: makeLogger("info"),
        /** Log a message with `console.log` */
        v: makeLogger("log"),
        /** Log a message with `console.warn` */
        w: makeLogger("warn"),
        /** What a Terrible Failure. Report a condition that should never happen, triggering a crash modal. */
        wtf: (msg: string) => {
            const jsLoadError = document.getElementById("js_load_error");
            if (jsLoadError) {
                const terribleFailure = <p style="color: #f00">
                    <br />
                    {msg} <br />
                    (<a href="https://codeberg.org/rini/Scarlet/issues/new" style="color: #f33; text-decoration: none">
                        please report this to SCARLET
                    </a>, not <span class="cheeky">TETR.IO</span>)
                </p>;
                jsLoadError.insertBefore(terribleFailure as Element, jsLoadError.lastChild);
                jsLoadError.classList.remove("handled");
            }
            throw Error(msg);
        },
    };
}

/**
 * Return a new `RegExp` from BRE syntax. BRE differs from standard syntax in which escapes for meta-characters are
 * inverted. This means, instead of writing `[a-z]` you write `\[a-z\]`, and instead of writing `function\(\)\{\}`,
 * you just write `function(){}`. This is particularly useful for applying regular expressions to source code.
 *
 * Note that there is no need to double escape, as template functions have access to the raw string:
 * {@link https://dev.mozilla.org/docs/Javascript_templates#raw_strings}
 *
 * You can specify flags by adding `(?flags)` to the start of the string. For example: ``bre`(?i)cat`.test("cAt")``.
 *
 * More information: {@link https://www.gnu.org/software/sed/manual/html_node/BRE-vs-ERE.html}
 */
export function bre(strings: TemplateStringsArray, ...subst: any[]) {
    const raw = String.raw(strings, ...subst);
    const flags = raw.match(/^\(\?[a-z]+\)/)?.[0] ?? "";
    // basically just add a backslash if there is an even amount of them, otherwise drop one
    const src = raw.slice(flags.length)
        .replace(/\\*[.*+?^${}()|[\]]/g, (m) =>
            m.length % 2 ? "\\" + m : m.slice(1))
        .replace(/\\i/g, "[A-Za-z_$][\\w$]*");

    return new RegExp(src, flags.slice(2, -1));
}
